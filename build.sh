#!/usr/bin/env bash
#途丁 服务发现组件
export LANG="en_US.UTF-8"
name="microservice/tuding-eureka"
main_port="8090"
expose_port="8090"
image_name=$(sudo docker images | grep ${name})
if [ -n "${image_name}" ];
then
#镜像存在
        container_name=$(sudo docker ps -a | grep ${name}|awk '{print $1}')
        if [ -n "${container_name}" ];
        then
                #容器存在
                sudo docker stop ${container_name} #停止容器
                sudo docker rm ${container_name} #移除容器
        fi
        latest_id=$(sudo docker images | grep ${name}|grep  'latest'|awk '{print $3}')
        image_id=$(sudo docker images | grep ${name}|awk '{print $3}')
        for i in ${image_id}
        do
                if [ ${i} != ${latest_id} ];then
                        sudo docker rmi -f ${i} #移除旧镜像
                fi
        done
        sudo docker  run -d -p ${main_port}:${expose_port}  --net host --privileged=true -v /home/tudinghotel/logs:/var/logs ${name}
else
    echo '镜像不存在，请检查构建过程....'
    exit 0
fi