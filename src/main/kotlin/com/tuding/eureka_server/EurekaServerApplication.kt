package com.tuding.eureka_server

import org.apache.log4j.Logger
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer

@SpringBootApplication
@EnableEurekaServer
class EurekaServerApplication


fun main(args: Array<String>) {
    SpringApplication.run(EurekaServerApplication::class.java, *args)
    val log: Logger = Logger.getLogger("eureka")
    log.info("eureka server started is completed ...")
}